import cv2
import numpy as np
from grip import GripPipeline



def main():

    print('Init camera')
    vid = cv2.VideoCapture(0)

    print('Creating pipeline')
    pipeline = GripPipeline()

    print('Running pipeline')
    while vid.isOpened():
        have_frame, frame = vid.read()
        if have_frame:
            pipeline.process(frame)
            frame2 = cv2.drawKeypoints(
                    pipeline.resize_image_output,
                    pipeline.find_blobs_output,
                    np.array([]),
                    (0,0,255),
                    cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
            frame3 = cv2.drawKeypoints(
                    pipeline.blur_output,
                    pipeline.find_blobs_output,
                    np.array([]),
                    (0,0,255),
                    cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

            cv2.imshow('blur result', frame3)
            cv2.imshow('blob detection', frame2)
        if cv2.waitKey(1) == 27: #esc
            break

    cv2.destroyAllWindows()
    print('Done.')


if __name__ == '__main__':
    main()
